FROM node:8.9-alpine
WORKDIR /app
COPY test.js ./
COPY package.json ./
RUN npm install
CMD node test.js