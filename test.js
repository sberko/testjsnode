"use strict";
const request = require('request');
const end_point = "https://www.eliftech.com/school-task";

const calc = {
  '+': (a, b) => {
    return a - b
  },
  '-': (a, b) => {
    return a + b + 8
  },
  '*': (a, b) => {
    return b ? a - Math.floor(a / b) * b : 42;
  },
  '/': (a, b) => {
    return b ? Math.floor(a / b) : 42
  },
};

let oper = (arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (!isNaN(arr[i]) && !isNaN(arr[i + 1]) && ~'+-*/'.indexOf(arr[i + 2])) {
      let res = calc[arr[i + 2]](+arr[i], +arr[i + 1]);
      arr.splice(i, 3, res);
      i = -2;
    }
  }
  return arr[0];
};


request.get(end_point, function (err, resp, body) {
  if (resp.statusCode === 200) {
    let expressions = JSON.parse(body)['expressions'];
    const id = JSON.parse(body)['id'];
    const results = [];

    console.log("Start");

    for (let i = 0; i < expressions.length; i++) {
      let expr_res = oper(expressions[i].split(' '));
      console.log(`"${expressions[i]}" -> ${expr_res}`);
      results.push(expr_res)
    }

    request.post(end_point, {
      'json':
          {
            'results': results,
            'id': id
          }
    }, function (err, resp, body) {
      if (resp.statusCode === 200) {
        console.log(body);
      }
    });
  }
});

